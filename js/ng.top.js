var naturesgateTop = (function(){

	function _init()
	{
		$pageTop = $('#page-top');

		//初期化
		$pageTop.hide();

		// スクロールトップの出現
		$(window).scroll(pageTop);

		// ページ内スクロール
		pageScroll();
	}


/*-------------
page-top-controll
-----------*/

		var pageTop = function() {
		//スクロールが400に達したら表示
		if($(this).scrollTop() > 400) {
			$pageTop.fadeIn();
		} else {
			$pageTop.fadeOut();
		}
	}


/*-------------
page-scroll
-----------*/

		var pageScroll = function() {
			$('a[href^=#]').not('#page-top a').on('click', function(){
			//クリックした要素の#を変数に入れる
			var Target = $(this.hash);
			//行き先までの画面上の高さの数値を変数に入れる
			var TargetOffset = $(Target).offset().top;
			//アニメーション時間ミリ秒
			var Time = 1000;
			//集めた情報を元にアニメーション
			$('html, body').animate({
				scrollTop: TargetOffset
			}, Time, "easeInOutCubic");
			return false;
		});
	}


	return {
		init : _init
	}

}());

$(function(){
	naturesgateTop.init();
});
